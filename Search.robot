*** settings ***
Library		SeleniumLibrary

*** Variables ***
${url_fastwork}		http://staging.fastwork.co/


*** Keywords ***
Open fastwork
	Open Browser	${url_fastwork}		browser=chrome
Login email	
	Click Element	//*[@id="login-link"]
	Input text		//input[@id='authorize-request-Credential']		uatfw.555@kobrandly.com
 	
Input password
 	Input text		//input[@id='signin-request-Password']		abcde1234

Search
	Input text		//input[@name="q" and @class="_fs-300"]		Software tester
	
*** Test cases ***
TC001 Login with email
	Open fastwork
	Search
	Click Element	//div[@class="icon -is-right _cs-pt _pdt-2px _cl-primary-500"]
	Wait Until Page Contains   		ทดสอบระบบ					10S