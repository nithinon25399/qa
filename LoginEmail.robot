*** settings ***
Library		SeleniumLibrary

*** Variables ***
${url_fastwork}		http://staging.fastwork.co/


*** Keywords ***
Open fastwork
	Open Browser	${url_fastwork}		browser=chrome
Login email	
	Click Element	//*[@id="login-link"]
	Input text		//input[@id='authorize-request-Credential']		uatfw.555@kobrandly.com
 	
Input password
 	Input text		//input[@id='signin-request-Password']			abcde1234
	
*** Test cases ***
TC001 Username blank
	Open fastwork
	Click Element	//*[@id="login-link"]
	Click button	//button[@class="tb-button -fluid _mgt-12px"]
	Wait Until Page Contains   		Username must not be blank					5s
	Close Browser

TC002 Incorrect username
	Open fastwork
	Click Element	//*[@id="login-link"]
	Input text		//input[@id='authorize-request-Credential']		fsadfsdf5@kobrandly.com
	Click button	//button[@class="tb-button -fluid _mgt-12px"]
	Wait Until Page Contains   		Incorrect username					5s
	Close Browser

TC003 Incorrect Password
	Open fastwork
	Login email
	Click button	//button[@class="tb-button -fluid _mgt-12px"]
	Input text		//input[@id='signin-request-Password']		dfsdfsd
	Click button	//button[@class="tb-button -fluid"]
	Wait Until Page Contains   		Incorrect Password					5S
	Close Browser

TC004 Login with email
	Open fastwork
	Login email
	Click button	//button[@class="tb-button -fluid _mgt-12px"]
	Input password
	Click button	//button[@class="tb-button -fluid"]
	Wait Until Page Contains   		เรามีฟรีแลนซ์มืออาชีพด้าน					10S
	Close Browser