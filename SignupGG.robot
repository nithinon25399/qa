*** settings ***
Library		SeleniumLibrary

*** Variables ***
${url_fastwork}		http://staging.fastwork.co/


*** Keywords ***
Open fastwork
	Open Browser	${url_fastwork}		browser=chrome
Sign up Google	
	Click Element			//*[@id="login-link"]
	Click Element			//a[contains(text(),'Register')]	
	Click Element			//a[@href = '/auth/google']
Input email
	Input text				//input[@id='identifierId']									uatfw.04@gmail.com

Input password
	Input text				//input[@class="whsOnd zHQkBf" and @name="password"]		nonzaza21

Checkbox 2
	Select Checkbox			//input[@id='post-TCConsent']
	Click button			//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]

Checkbox 3
	Select Checkbox			//input[@id='post-PPConsent']
	Click button			//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]

Checkbox 4
	Select Checkbox			//input[@id='post-MKTConsent']
	Click button			//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]

Checkbox 5
	Select Checkbox			//input[@id='post-TCConsent']
	Select Checkbox			//input[@id='post-MKTConsent']
	Click button			//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]

Checkbox 6
	Select Checkbox			//input[@id='post-PPConsent']
	Select Checkbox			//input[@id='post-MKTConsent']
	Click button			//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]

Checkbox 7
	Select Checkbox			//input[@id='post-TCConsent']
	Select Checkbox			//input[@id='post-PPConsent']
	

Checkbox 8
	Select Checkbox			//input[@id='post-TCConsent']
	Select Checkbox			//input[@id='post-PPConsent']
	Select Checkbox			//input[@id='post-MKTConsent']
	

*** Test cases ***

TC001 Uncheck all
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Click button					//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]

TC002 Checkbox T&C
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Checkbox 2

TC003 Checkbox Privacy policy
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Checkbox 3

TC004 Checkbox News
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Checkbox 4

TC005 Checkbox T&C, News
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Checkbox 5

TC006 Checkbox Privacy Policy, News
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Checkbox 6	

TC007 Checkbox T&C, Privacy Policy
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Checkbox 7	
	Click button					//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]
	Wait Until Page Contains   		เรามีฟรีแลนซ์มืออาชีพด้าน					10S
	
TC008 Signup with google
	Open fastwork
	Sign up Google
	Input email
	Click Element					//div[@id='identifierNext']/div/button/span
	Wait Until Element Is Visible	//input[@class="whsOnd zHQkBf" and @name="password"]		5s
	Input password	
	Click Element					//div[@id="passwordNext"]
	Wait Until Element Is Visible	//input[@id='post-TCConsent']		5s
	Checkbox 8
	Click button					//button[@class="signup-btn tb-button -fluid _ffml-kanit btn-ff"]
	Wait Until Page Contains   		เรามีฟรีแลนซ์มืออาชีพด้าน					10S
	