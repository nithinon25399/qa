*** settings ***
Library		SeleniumLibrary

*** Variables ***
${url_fastwork}		http://staging.fastwork.co/


*** Keywords ***
Open fastwork
	Open Browser	${url_fastwork}		browser=chrome
Sign up fastwork	
	Click Element	//a[@id="login-link"]	
	Click Element	//a[@href = '/oauth/signup']
Fill information
	Input text		//input[@id='signup-request-username' and @name='username']						testqa35
	Input text		//input[@id='signup-request-first_name' and @name='first_name']					สันชัย
	Input text		//input[@id='signup-request-last_name' and @name='last_name']					ใจสู้
	Input text		//input[@id='signup-request-email' and @name='email']							uatfw78@coooooool.com
	Input text		//input[@id='signup-request-password' and @name='password']						abcde1234
	Input text		//input[@id='signup-request-confirm_password' and @name='confirm_password']		abcde1234
	Input text		//input[@id='signup-request-phone_number' and @name='phone_number']				0844488835

existing username
	Input text		//input[@id='signup-request-username' and @name='username']						testqa15
	Input text		//input[@id='signup-request-first_name' and @name='first_name']					สันชัย
	Input text		//input[@id='signup-request-last_name' and @name='last_name']					ใจสู้
	Input text		//input[@id='signup-request-email' and @name='email']							uatfw.07@block521.com
	Input text		//input[@id='signup-request-password' and @name='password']						abcde1234
	Input text		//input[@id='signup-request-confirm_password' and @name='confirm_password']		abcde1234
	Input text		//input[@id='signup-request-phone_number' and @name='phone_number']				0844488865

existing email
	Input text		//input[@id='signup-request-username' and @name='username']						testqa19
	Input text		//input[@id='signup-request-first_name' and @name='first_name']					สันชัย
	Input text		//input[@id='signup-request-last_name' and @name='last_name']					ใจสู้
	Input text		//input[@id='signup-request-email' and @name='email']							uatfw08@popcornfly.com
	Input text		//input[@id='signup-request-password' and @name='password']						abcde1234
	Input text		//input[@id='signup-request-confirm_password' and @name='confirm_password']		abcde1234
	Input text		//input[@id='signup-request-phone_number' and @name='phone_number']				0844488859

existing mobile number
	Input text		//input[@id='signup-request-username' and @name='username']						testqa20
	Input text		//input[@id='signup-request-first_name' and @name='first_name']					สันชัย
	Input text		//input[@id='signup-request-last_name' and @name='last_name']					ใจสู้
	Input text		//input[@id='signup-request-email' and @name='email']							uatfw055@waterisgone.com
	Input text		//input[@id='signup-request-password' and @name='password']						abcde1234
	Input text		//input[@id='signup-request-confirm_password' and @name='confirm_password']		abcde1234
	Input text		//input[@id='signup-request-phone_number' and @name='phone_number']				0844488888

*** Test cases ***
TC001 existing username
	Open fastwork
	Sign up fastwork
	existing username
	Select Checkbox						//input[@id='post-TCConsent']
	Select Checkbox						//input[@id='post-PPConsent']
	Click button						//button[@class="signup-btn tb-button -fluid _mgt-24px"]
	Wait Until Page Contains   			username already exist					5s
	Close Browser

TC002 existing email
	Open fastwork
	Sign up fastwork
	existing email
	Select Checkbox						//input[@id='post-TCConsent']
	Select Checkbox						//input[@id='post-PPConsent']
	Click button						//button[@class="signup-btn tb-button -fluid _mgt-24px"]
	Wait Until Page Contains   			email already exist					5s
	Close Browser

TC003 existing mobile number
	Open fastwork
	Sign up fastwork
	existing mobile number
	Select Checkbox						//input[@id='post-TCConsent']
	Select Checkbox						//input[@id='post-PPConsent']
	Click button						//button[@class="signup-btn tb-button -fluid _mgt-24px"]
	Wait Until Page Contains   			phone number already exist					5s
	Close Browser

TC004 Sign up fastwork
	Open fastwork
	Sign up fastwork
	Fill information
	Select Checkbox						//input[@id='post-TCConsent']
	Select Checkbox						//input[@id='post-PPConsent']
	Click button						//button[@class="signup-btn tb-button -fluid _mgt-24px"]
	Wait Until Page Contains   			เรามีฟรีแลนซ์มืออาชีพด้าน			timeout= 20s